# Culqui - Backend Technical Test

## Getting started

1.- Install the dependencies and devdependencies
````
npm install
````

2.- Build the project so the code turns from typescript into javascript
````
npm run build 
````

`Note:` Run this command every time you make a change

## Start the project

### Run LocalHost
If you want to run the project in your localhost, run the following command
````
npm run dev 
````
`Note:` Database Mongo DB is Online

### Deploy AWS
If you want to deploy the project directly in AWS, run the following command
````
npm run deploy
````
`Note:` You need to have an active IAM AWS account 

## Testing

### Run The Unit Tests
If you want to check the unit tests for this project, run the following command
````
npm run test
````

## Solution 
### API Online 

```
https://et17lvqh77.execute-api.us-east-1.amazonaws.com/Prod
```

### API Responses

`GET | https://et17lvqh77.execute-api.us-east-1.amazonaws.com/Prod/v1/token?token=3XDpVQZJ55eF9pRH`

Authorzation - Baerer Token: 
```
pk_test_0ae8dW2FpEAZlxlz
```

Response
````json
{
  "timestamp": "2023-07-04T06:44:41.035Z",
  "message": "Missing the API Key. Did not set the Authorization into the headers, but not following the parttern. \"Baerer YOUR_API_KEY\"",
  "statusCode": 401
}
````

`POST | https://et17lvqh77.execute-api.us-east-1.amazonaws.com/Prod/v1/token`

Authorzation - Baerer Token: 
```
pk_test_0ae8dW2FpEAZlxlz
```

Body: 
```json

{
    "cvv": 333,
    "card_number": 4556364607935616,
    "expiration_month": "8",
    "expiration_year": "2026",
    "email": "genaro@hotmail.com"
}

```

Response: 
```json
{
    "token": "MbVs37H7VZfpSFJv"
}
```
`Note:` The response will not be the same because every token is unique. It's just a example of how the response would look like

## Author
Genaro Martinez

genaro.martinez@delfosti.com