import { ITokenRequestCreateOne } from '../../common/interfaces/requests/token.request.js';

export default class CreateTokenRequest {
  cvv: number;
  email: string;
  card_number: number;
  expiration_year: string;
  expiration_month: string;

  constructor(params: {});

  constructor(params: ITokenRequestCreateOne) {
    this.cvv = params.cvv;
    this.email = params.email;
    this.card_number = params.card_number;
    this.expiration_year = params.expiration_year;
    this.expiration_month = params.expiration_month;
  }
}
