// * HTTP STATUS CODES
export const HTTP_STATUS_201 = 201;
export const HTTP_STATUS_200 = 200;
export const HTTP_STATUS_400 = 400;
export const HTTP_STATUS_401 = 401;
export const HTTP_STATUS_404 = 404;
export const HTTP_STATUS_409 = 409;
export const HTTP_STATUS_500 = 500;
export const HTTP_STATUS_405 = 405;

// * CUSTOM ERROR MESSAGES
export const NO_HEADERS = 'Missing the headers. Please add them correctly'
export const NOT_API_KEY = 'Missing the API Key. Did not set the Authorization into the headers, but not following the parttern. "Baerer YOUR_API_KEY"'
export const NOT_VALID_API_KEY = 'The API Key passed is not correct. Make sure it is following the correct format'

// * AUTHORIZATION
export const AUTHORIZATION_TYPE = 'Baerer'
export const AUTHORIZATION_LENGTH = 24

// * TOKEN
export const TOKEN_EXPIRATION = 15
