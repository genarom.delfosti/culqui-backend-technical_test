import mongoose from 'mongoose';

export const tokenSchema = new mongoose.Schema({
  email: {
    trim: true,
    type: String,
    require: true,
  },
  expired: {
    type: Boolean,
    require: true,
  },
  card_number: {
    trim: true,
    type: String,
    require: true,
  },
  expiration_date_token: {
    type: Date,
    require: true,  
  },
  expiration_year: {
    type: String,
    require: true,
  },
  expiration_month: {
    type: String,
    require: true,
  },
  auto_generated_token: {
    type: String,
    require: true,
  },
});

export default mongoose.model('token', tokenSchema);

export interface Token {
  cvv: number;
  _id?: string;
  email: string;
  expired: boolean;
  card_number: string;
  expiration_year: string;
  expiration_month: string;
  expiration_date_token: Date;
  auto_generated_token: string;
}
