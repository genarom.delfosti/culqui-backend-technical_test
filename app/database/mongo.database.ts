import mongoose from 'mongoose';

const mongoUriLocal = 'mongodb+srv://admin:WIRkAK7msT30LzYD@samdb.16haywr.mongodb.net/';
let connection: any = null;

export const connectDatabase = async () => {
  if (connection != null) return;

  connection = await mongoose.connect(mongoUriLocal, { serverSelectionTimeoutMS: 5000 });
  return connection;
};
