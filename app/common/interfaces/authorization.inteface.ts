export interface IAuthBodyException {
  message: string;
  timestamp: Date;
  statusCode: number;
}