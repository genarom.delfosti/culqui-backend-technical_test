export interface ITokenRequestCreateOne {
  cvv: number;
  email: string;
  card_number: number;
  expiration_month: string;
  expiration_year: string;
}

export interface ITokenRequestFindOneByAutoGeneretedToken {
  token: string
}