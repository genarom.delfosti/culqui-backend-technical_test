export type TEndpoint = 
  | 'POST|/v1/token'
  | 'GET|/v1/token'