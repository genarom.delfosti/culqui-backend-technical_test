import middy from 'middy';
import { Context, APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';

import * as Constants from '../constants.js';
import { IAuthBodyException } from '../common/interfaces/authorization.inteface.js';
import { validateApiKey } from './utils/authorization.utils.js';

export const authorizationMiddleware = (): middy.MiddlewareObject<
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
  Context
> => {
  const beforeMiddleware: middy.MiddlewareFunction<APIGatewayProxyEvent, APIGatewayProxyResult, Context> = async (
    req,
  ): Promise<void> => {
    const { headers } = req.event;

    if (!headers) {
      const body: IAuthBodyException = {
        message: Constants.NO_HEADERS,
        timestamp: new Date(),
        statusCode: Constants.HTTP_STATUS_401,
      };

      const bodyString = JSON.stringify(body);

      const errorResponse: APIGatewayProxyResult = {
        body: bodyString,
        statusCode: Constants.HTTP_STATUS_401,
      };

      req.response = errorResponse;
      return;
    }

    const authorization = headers?.authorization || headers?.Authorization;

    if (!authorization) {
      const body: IAuthBodyException = {
        timestamp: new Date(),
        message: Constants.NOT_API_KEY,
        statusCode: Constants.HTTP_STATUS_401,
      };

      const bodyString = JSON.stringify(body);

      const errorResponse: APIGatewayProxyResult = {
        body: bodyString,
        statusCode: Constants.HTTP_STATUS_401,
      };

      req.response = errorResponse;
      return;
    }

    const [type, apiKey] = authorization.split(' ');

    const isTypeCorrect = type == Constants.AUTHORIZATION_TYPE;
    const isApiKeyLengthCorrect = apiKey?.length == Constants.AUTHORIZATION_LENGTH;
    const isApiKeyCorrect = validateApiKey(apiKey);

    const isValidAuth = isTypeCorrect || isApiKeyLengthCorrect || isApiKeyCorrect;

    if (!isValidAuth) {
      const body: IAuthBodyException = {
        timestamp: new Date(),
        message: Constants.NOT_VALID_API_KEY,
        statusCode: Constants.HTTP_STATUS_401,
      };

      const bodyString = JSON.stringify(body);

      const errorResponse: APIGatewayProxyResult = {
        body: bodyString,
        statusCode: Constants.HTTP_STATUS_401,
      };

      req.response = errorResponse;
      return;
    }
  };

  return { before: beforeMiddleware };
};
