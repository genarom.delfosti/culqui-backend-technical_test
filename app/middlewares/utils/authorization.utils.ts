export const validateApiKey = (apiKey: string): boolean => {
  if(!apiKey) return false

  const regex: RegExp = /^pk_test_[a-zA-Z0-9]{16}$/;
  return regex.test(apiKey);
}