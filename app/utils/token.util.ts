export const generateToken = () => {
  const characters = '0123456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz';
  const tokenLength = 16;

  let token = '';

  for (let i = 0; i < tokenLength; i++) {
    const random = Math.random();
    const characterLength = characters.length;
    const randomIndexAux = random * characterLength;
    const randomIndex = Math.floor(randomIndexAux);
    const charAux = characters.charAt(randomIndex);
    token += charAux;
  }

  return token;
};
