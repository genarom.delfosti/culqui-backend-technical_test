export const maskCardNumber = (card: string) => {
  const regex = new RegExp('[a-zA-Z]');

  const greaterThanSixteen = card.length > 16;
  const lowerThanThirteen = card.length < 13;
  const hasLetters = regex.test(card);

  const notValidCard = greaterThanSixteen || lowerThanThirteen || hasLetters;

  if (notValidCard) {
    throw new Error('Not valid card');
  }

  const firstCardDigits = card.slice(0, 6);
  const lastCardDigits = card.slice(-4);
  const maskedCardDigits = card.slice(6, -4).replace(/\d/g, '*');
  
  const cardMasked = `${firstCardDigits}${maskedCardDigits}${lastCardDigits}`;
  return cardMasked;
};
