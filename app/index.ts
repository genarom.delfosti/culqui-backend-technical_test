import { Handler, APIGatewayProxyEvent, Context } from 'aws-lambda';
import { Container } from 'inversify';
import middy from 'middy';
import TokenController from './controllers/token.controller.js';
import TokenService from './services/token.service.js';
import { ENDPOINT_RESPONSE } from './utils/endpoints.util.js';
import { authorizationMiddleware } from './middlewares/authorization.middleware.js';

export const handler: Handler = middy(async (event: APIGatewayProxyEvent, context: Context) =>
  handlerMiddy(event, context),
)

const handlerMiddy = async (event: APIGatewayProxyEvent, context: Context) => {
  const container: Container = new Container();

  container.bind(TokenService.name).to(TokenService);
  container.bind(TokenController.name).to(TokenController);

  context.callbackWaitsForEmptyEventLoop = false;

  const HTTP_METHOD = event?.httpMethod || event?.requestContext?.httpMethod;

  const HTTP_PATH = event?.path || event?.requestContext?.path;

  const ENDPOINT = `${HTTP_METHOD}|${HTTP_PATH}`;

  return ENDPOINT_RESPONSE(container, ENDPOINT, event);
};
