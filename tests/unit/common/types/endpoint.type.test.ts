import { describe, expect, test } from '@jest/globals';
import { TEndpoint } from '../../../../app/common/types/endpoint.type';

describe("Endpoint Type", () => {
  test('POST|/v1/token', () => {
    const endpoint: TEndpoint = "POST|/v1/token"

    expect(endpoint).toBeDefined()
    expect(typeof endpoint).toBe("string")
    expect(endpoint).toEqual("POST|/v1/token")
  })

  test('GET|/v1/token', () => {
    const endpoint: TEndpoint = "GET|/v1/token"

    expect(endpoint).toBeDefined()
    expect(typeof endpoint).toBe("string")
    expect(endpoint).toEqual("GET|/v1/token")
  })
})