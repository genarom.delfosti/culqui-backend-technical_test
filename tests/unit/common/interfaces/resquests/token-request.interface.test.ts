import { describe, expect, test } from "@jest/globals"
import { ITokenRequestCreateOne, ITokenRequestFindOneByAutoGeneretedToken } from "../../../../../app/common/interfaces/requests/token.request"

describe("Token Request Interface", () => {
  const tokenRequestCreateOne: jest.MockedObject<ITokenRequestCreateOne> = {
    cvv: 123,
    email: "email",
    card_number: 12345689101,
    expiration_month: "expiration_month",
    expiration_year: "expiration_year",
  };

  const tokenRequestFindOneByAutoGeneretedToken: jest.MockedObject<ITokenRequestFindOneByAutoGeneretedToken> = {
    token: "token"
  };

  test("Should have all the properties | ITokenRequestCreateOne", () => {
    expect(tokenRequestCreateOne).toHaveProperty('cvv')
    expect(tokenRequestCreateOne).toHaveProperty('email')
    expect(tokenRequestCreateOne).toHaveProperty('card_number')
    expect(tokenRequestCreateOne).toHaveProperty('expiration_year')
    expect(tokenRequestCreateOne).toHaveProperty('expiration_month')
  })

  test("Should have all the properties | ITokenRequestFindOneByAutoGeneretedToken", () => {
    expect(tokenRequestFindOneByAutoGeneretedToken).toHaveProperty('token')
  })
})