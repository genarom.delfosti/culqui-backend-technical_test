import { describe, expect, test } from '@jest/globals';

import { IResponseTokenCreateOne, IResponseTokenFindOneByAutoGeneratedToken } from '../../../../../app/common/interfaces/responses/token-response.interface';

describe("Token Response Interface", () => {
  const responseTokenCreateOne: jest.MockedObject<IResponseTokenCreateOne> = {
    token: "123"
  }

  const responseTokenFindOneByAutoGeneratedToken: jest.MockedObject<IResponseTokenFindOneByAutoGeneratedToken> = {
    cvv: 123,
    _id: "_id",
    expired: false,
    email: "email",
    card_number: "card_number",
    expiration_date_token: new Date(),
    expiration_year: "expiration_year",
    expiration_month: "expiration_month",
    auto_generated_token: "auto_generated_token",
  }

  test("Should have all the properties | IResponseTokenCreateOne", () => {
    const tokenLength = responseTokenCreateOne.token.length
    
    expect(responseTokenCreateOne).toBeDefined()
    expect(responseTokenCreateOne).toHaveProperty("token")
    expect(tokenLength).not.toBe(16)
  })

  test("Should have all the proporties | IResponseTokenFindOneByAutoGeneratedToken", () => {
    expect(responseTokenFindOneByAutoGeneratedToken).toHaveProperty("_id")
    expect(responseTokenFindOneByAutoGeneratedToken).toHaveProperty("cvv")
    expect(responseTokenFindOneByAutoGeneratedToken).toHaveProperty("email")
    expect(responseTokenFindOneByAutoGeneratedToken).toHaveProperty("expired")
    expect(responseTokenFindOneByAutoGeneratedToken).toHaveProperty("card_number")
    expect(responseTokenFindOneByAutoGeneratedToken).toHaveProperty("expiration_year")
    expect(responseTokenFindOneByAutoGeneratedToken).toHaveProperty("expiration_month")
    expect(responseTokenFindOneByAutoGeneratedToken).toHaveProperty("expiration_date_token")
    expect(responseTokenFindOneByAutoGeneratedToken).toHaveProperty("auto_generated_token")
  })
})