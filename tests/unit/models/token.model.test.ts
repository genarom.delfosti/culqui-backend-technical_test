import { describe, expect, test } from '@jest/globals';
import tokenModel, { Token, tokenSchema } from '../../../app/models/token.model';
import mongoose from 'mongoose';

describe("Token Model", () => {
  const token: Token = {
    cvv: 111,
    expired: false,
    expiration_month: "7",
    expiration_year: "2025",
    card_number: "455636******5616",
    email: "genaro010204@gmail.com",
    expiration_date_token: new Date("2023-07-03T02:15:28.648Z"),
    auto_generated_token: "4qbg2r3oS4QasfpC",
  }

  test("Should be defined", () => {
    expect(token).toBeDefined()
  })

  test("Should accepts the params needed", () => {
    expect(token).toEqual({
      cvv: 111,
      expired: false,
      expiration_month: "7",
      expiration_year: "2025",
      card_number: "455636******5616",
      email: "genaro010204@gmail.com",
      expiration_date_token: new Date("2023-07-03T02:15:28.648Z"),
      auto_generated_token: "4qbg2r3oS4QasfpC",
    })
  })

  test("Should have token as modelName", () => {
    expect(tokenModel.modelName).toBe("token")
  })

  test("Should be instance of mongoose.Schema", () => {
    expect(tokenSchema).toBeInstanceOf(mongoose.Schema)
  })
})