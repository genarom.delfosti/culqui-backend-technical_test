import { describe, expect, test } from "@jest/globals"

import { maskCardNumber } from "../../../app/utils/card.util"

describe("Card Util", () => {
  test("Should be masked card number", () => {
    const cardNumber = 4556364607935616
    const cardNumberString = cardNumber.toString()
    
    const cardMasked = maskCardNumber(cardNumberString)

    expect(cardMasked).toBeDefined()
    expect(typeof(cardMasked)).toBe("string")
    expect(cardMasked).toBe("455636******5616")
  })

  test("Should throw error", () => {
    const cardNumber = 4556364607935616
    const cardNumberString = cardNumber.toString()
    
    const regex = new RegExp('[a-zA-Z]');
    const hasLetters = regex.test(cardNumberString);

    expect(cardNumberString.length).not.toBeGreaterThan(16)
    expect(cardNumberString.length).not.toBeLessThan(13)
    expect(hasLetters).not.toBeTruthy()
  })
})